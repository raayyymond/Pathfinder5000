#include <MsTimer2.h>

#define signalIn 2
volatile unsigned long pulseLength = 0;
volatile unsigned long lastChange = 0;
bool cool = 1;
void setup(){
  MsTimer2::set(200, flash); // 500ms period
  MsTimer2::start();
  pinMode(9, OUTPUT);
  pinMode(signalIn, INPUT);
  attachInterrupt(digitalPinToInterrupt(signalIn), pulsed, CHANGE);
  Serial.begin(9600);
}
void flash(){
  digitalWrite(9, cool);
  cool = !cool;
}
void loop(){
}

void pulsed() {
  pulseLength = millis() - lastChange;
  lastChange = millis();
  Serial.print("Pulse Length: ");
  Serial.print(pulseLength);
  Serial.println(" ms");
}

