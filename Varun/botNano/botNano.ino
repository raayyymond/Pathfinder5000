#include <SoftwareSerial.h>

#define motor1 8
#define motor2 5
#define rotary1 A0
#define rotary2 A1

int stepsPerCM1 = 22;
int stepsPerCM2 = 22;
String serialData;

SoftwareSerial MCU(10,11);

void setup() {
  pinMode(motor1, OUTPUT);
  pinMode(motor1 + 1, OUTPUT);
  pinMode(motor1 - 1, OUTPUT);
  pinMode(motor2, OUTPUT);
  pinMode(motor2 + 1, OUTPUT);
  pinMode(motor2 - 1, OUTPUT);
  Serial.begin(9600); 
  MCU.begin(9600);
}

void loop() {
  if (MCU.available()){
    while (MCU.available()){
      serialData = serialData + MCU.read();
    }
    if((serialData.length() > 7) && (serialData.substring(0,3).toInt() == 't' || serialData.substring(0,3).toInt() == 'm')){
      char movement = serialData.substring(0,3).toInt();
      char direc = serialData.substring(3,6).toInt();
      int posOfDot = 0;
      for (int i = 6; i < serialData.length()-2; i++){
        if(serialData.substring(i, i+2).toInt() == 46){
          posOfDot = i;
          break;
        }
      }
      int distance = serialData.substring(posOfDot+2).toInt();
      if (movement == 'm'){
          actMove(direc == 'f' ? 1 : 0, distance);
          //String s = "move "; s = s + direc + " "; s = s + distance;Serial.println(s);
      }
    /*Serial.print("movemnt type : ");
    Serial.println(movement);
    Serial.print("direction  : ");
    Serial.println(direc);
    Serial.print("distance  : ");
    Serial.println(distance);
    Serial.println();
    MCU.print("done");*/
    }
    serialData = "";
  }
}

void actMove(bool forward, int distance) {
  for(int i = 0; i < distance; i++) {
    stepsPerCM1 = i % 50 == 0 ? 23 : 22;
    moveNGrove(forward, 1);
    delay(400);
  }
}

void moveNGrove(bool forward, int distance) {
  bool goalReached = false;
  int steps = 0;
  int steps2 = 0;
  int motorSpeed1 = 155;
  int motorSpeed2 = 155;
  bool wasLow = analogRead(rotary1) < 150;
  bool wasLow2 = analogRead(rotary2) < 150;
  digitalWrite(!forward ? motor1 - 1 : motor1, 1);
  digitalWrite(!forward ? motor2 - 1 : motor2, 1);
  analogWrite(motor1+1, motorSpeed1);
  analogWrite(motor2+1, motorSpeed2);
  while (!goalReached) {
    if (wasLow) {
      if (analogRead(rotary1) > 150) {
        steps++;
        wasLow = 0;
      }
    } else if (analogRead(rotary1) < 150) {
      wasLow = 1;
    }
    if (wasLow2) {
      if (analogRead(rotary2) > 150) {
        steps2++;
        wasLow2 = 0;
      }
    } else if (analogRead(rotary2) < 150) {
      wasLow2 = 1;
    }
    if(steps > steps2){
      motorSpeed1 -= 1;
      motorSpeed2 = 155;
    } 
    if(steps2 > steps){
      motorSpeed2 -= 1;
      motorSpeed1 = 155;
    }
    if(distance*stepsPerCM1 < steps){
      motorSpeed1 = 0; 
    }
    if(distance*stepsPerCM2 < steps2){
      motorSpeed2 = 0;
    }
    if (distance*stepsPerCM1 < steps && distance*stepsPerCM2 < steps2){
      goalReached = 1;
    }
    analogWrite(motor1+1, motorSpeed1);
    analogWrite(motor2+1, motorSpeed2);
  }
  analogWrite(motor1, 0);
  analogWrite(motor2, 0);
}
/*
void turn(bool forward, int angle) {
  bool goalReached = false;
  int steps = 0;
  int steps2 = 0;
  int motorSpeed1 = 155;
  int motorSpeed2 = 155;
  bool wasLow = analogRead(rotary1) < 150;
  bool wasLow2 = analogRead(rotary2) < 150;
  digitalWrite(forward ? motor1 - 1 : motor1, 1);
  digitalWrite(!forward ? motor2 - 1 : motor2, 1);
  analogWrite(motor1+1, motorSpeed1);
  analogWrite(motor2+1, motorSpeed2);
  while (!goalReached) {
    if (wasLow) {
      if (analogRead(rotary1) > 150) {
        steps++;
        wasLow = 0;
      }
    } else if (analogRead(rotary1) < 150) {
      wasLow = 1;
    }
    if (wasLow2) {
      if (analogRead(rotary2) > 150) {
        steps2++;
        wasLow2 = 0;
      }
    } else if (analogRead(rotary2) < 150) {
      wasLow2 = 1;
    }
    if(steps > steps2){
      motorSpeed1 -= 1;
      motorSpeed2 = 155;
    } 
    if(steps2 > steps){
      motorSpeed2 -= 1;
      motorSpeed1 = 155;
    }
    if(distance*stepsPerCM1 < steps){
      motorSpeed1 = 0; 
    }
    if(distance*stepsPerCM2 < steps2){
      motorSpeed2 = 0;
    }
    if (angle < steps && angle < steps2){
      goalReached = 1;
    }
    analogWrite(motor1+1, motorSpeed1);
    analogWrite(motor2+1, motorSpeed2);
  }
  analogWrite(motor1, 0);
  analogWrite(motor2, 0);
}*/
