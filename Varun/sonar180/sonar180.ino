#include <Servo.h>
#define SERVO_PIN 5
#define SONAR_1_PIN 13
#define SONAR_2_PIN 10
int degree;
int distances[360];
Servo sonarServo;
void setup() {
  sonarServo.attach(SERVO_PIN);
  Serial.begin(9600);
  /*sonarServo.write(90);
  delay(1000);
  sonarServo.write(0);
  delay(1000);
  sonarServo.write(179);
  delay(1000);
  sonarServo.write(90);*/
  pinMode(13,OUTPUT);
}

void loop() {
}

void ck(){
  for(degree = 0; degree < 180; degree+=5){
     sonarServo.write(degree);
     distances[degree] = getDistance(SONAR_1_PIN);
     distances[degree+180] = getDistance(SONAR_2_PIN);
     delay(60);
  }
}

int getDistance(int pin){
  digitalWrite(pin,HIGH);
  delayMicroseconds(10);
  digitalWrite(pin,LOW);
  return pulseIn(pin-1,HIGH);
}
