void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("Raymonds the biggest looser");
  delay(100);
  Serial.print("f");
  delay(100);
  moveBot(true, 906);
  delay(100);
  moveBot(false, 96);
  delay(100);
  moveBot(1, 3);
  delay(100);
  turn(0, 10);
  delay(200);
}

bool moveBot(bool forward, int distance){
  String returnMessage = "";
  Serial.print('m');
  Serial.print(forward ? 'f' : 'r');
  Serial.print('.');
  char distChars[String(distance).length()];
  strcpy(distChars, String(distance).c_str());
  for(char c : distChars) {
    Serial.print((char) String(c).toInt());
  }
  delay(500*distance);
  while(!Serial.available()){}
  while(Serial.available){
    returnMessage = returnMessage + Serial.read();
  }
  return = returnMessage.equals("true");
}
void turn(bool forward, int degree){
  Serial.print('t');
  Serial.print(forward ? 'f' : 'r');
  Serial.print('.');
  char distChars[String(degree).length()];
  strcpy(distChars, String(degree).c_str());
  for(char c : distChars) {
    Serial.print((char) String(c).toInt());
  }
}

