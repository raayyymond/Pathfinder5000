boolean robotIsScanning = false;
const int solarThreshold = 75;
unsigned long lastSend = 0;

void setup() {
  Serial.begin(115200);
  setupWifi();
}

void loop () {
  if(hasMessage()) {
    String recieved = getMessage();
    if(recieved.equals("scanning")) {
      robotIsScanning = true;
    } else if(recieved.equals("finished")) {
      robotIsScanning = false;
    }
  }

  if(robotIsScanning) {
    if(analogRead(A0) > solarThreshold) {
      Serial.println("laser on panel");
      unsigned long current = millis();
      if(current - lastSend > 1000) {
        lastSend = current;
        sendMessage("laser hit");
      }
    }
  }
}
