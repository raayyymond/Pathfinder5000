import java.util.HashMap;
import java.util.Map;

public class Node implements Comparable<Node> {

    private static Map<Location, Node> nodes = new HashMap<>();

    public static Node getNode(Location location) {
        if(!nodes.containsKey(location)) {
            nodes.put(location, new Node(location));
        }
        return nodes.get(location);
    }

    private Node parent;
    private Location location;
    private int fCost   , hCost;

    public int gethCost() {
        return hCost;
    }

    public void sethCost(int hCost) {
        this.hCost = hCost;
    }

    private Node(Location location) {
        this.location = location;
        sethCost(Math.abs(location.getX() - MazeProjector.GOAL.getX()) + Math.abs(location.getY() - MazeProjector.GOAL.getY()));
    }

    public void setParent(Node node) {
        parent = node;
    }

    public Location getLocation() {
        return location;
    }

    public int getfCost() {
        return fCost;
    }

    public void setfCost(int fCost) {
        this.fCost = fCost;
    }

    public Node getParent() {
        return parent;
    }

    @Override
    public int compareTo(Node node) {
        return getfCost() - node.getfCost();
    }
}
