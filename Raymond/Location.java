public class Location {

    private int x;
    private int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Location add(int x, int y) {
        return new Location(getX() + x, getY() + y);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Location))
            return false;
        Location loc = (Location) obj;
        return loc.getY() == getY() && loc.getX() == getX();
    }

    public String getId() {
        return toString();
    }

    public static Location fromString(String string) {
        return new Location(Integer.valueOf(string.split(" ")[0]), Integer.valueOf(string.split(" ")[1]));
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}
