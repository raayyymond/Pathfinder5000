import java.util.*;

public class MazeProjector {

    static int GRID_SIZE = 5;
    static int ROBOT_WIDTH = 2;
    static char[][] MAP;
    static Location GOAL;

    private static TreeSet<Node> open;
    private static List<Node> closed;

    public static void main(String[] args) throws InterruptedException {
        //setup array
        int[] distances = new int[360];
        Arrays.fill(distances, 0);

        //fill array with circle points in distance array
        /*for (int i = 0; i < 360; i += 32)
            distances[i] = 10;*/
                /*fakeData(0, 200, 10, 100, 360);*/

        //setup MAP / plane
        MAP = new char[GRID_SIZE][GRID_SIZE];
        for (char[] row : MAP)
            Arrays.fill(row, ' ');


        //convert polar to cartesian coordinates, add to MAP as 'X', add to readings list as coordinate
        List<Location> readings = new ArrayList<>();
        for (int i = 0; i < distances.length; i++) {
            int x = (int) Math.round(distances[i] * Math.cos(Math.toRadians(i)));
            int y = (int) Math.round(distances[i] * Math.sin(Math.toRadians(i)));

            if (MAP[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != 'X')
                readings.add(new Location(x, y));

            MAP[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'X';
        }


        //generate circle around each point and draw line to any X's within the circle
        List<Line> lines = new ArrayList<>();
        for (Location loc : readings) {
            List<Location> circle = generateCircleCoords(ROBOT_WIDTH, loc);

            //TODO: detect objects in circle and add lines
            for (Location circleLoc : circle) {
                if (circleLoc.getX() + GRID_SIZE / 2 < 0 || circleLoc.getX() + GRID_SIZE / 2 >= MAP.length ||
                        circleLoc.getY() + GRID_SIZE / 2 < 0 || circleLoc.getY() + GRID_SIZE / 2 >= MAP[0].length)
                    continue;

                if (MAP[circleLoc.getX() + GRID_SIZE / 2][circleLoc.getY() + GRID_SIZE / 2] == 'X') {
                    if (loc.equals(circleLoc)) continue;
                    Line line = new Line(loc, circleLoc);
                    lines.add(line);
                }
            }
        }


        //for each line draw line on MAP with A's
        for (Line line : lines) {
            drawLine(line, MAP);
        }


        //for each dot on MAP draw circle
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                if (MAP[x][y] == 'A' || MAP[x][y] == 'X') {
                    List<Location> circleLocs = generateCircleCoords(ROBOT_WIDTH / 2 - 1, new Location(x, y));
                    for (Location circleLoc : circleLocs) {
                        if (circleLoc.getX() < 0 || circleLoc.getX() >= GRID_SIZE) continue;
                        if (circleLoc.getY() < 0 || circleLoc.getY() >= GRID_SIZE) continue;
                        if (MAP[circleLoc.getX()][circleLoc.getY()] != ' ') continue;
                        MAP[circleLoc.getX()][circleLoc.getY()] = '.';
                    }
                }
            }
        }

        //data for beacon, draw beacon
        int heading = 45;
        int distance = 3;
        int beaconX = (int) Math.round(distance * Math.cos(Math.toRadians(heading)));
        int beaconY = (int) Math.round(distance * Math.sin(Math.toRadians(heading)));
        GOAL = new Location(beaconX + GRID_SIZE / 2, beaconY + GRID_SIZE / 2);
        MAP[beaconX + GRID_SIZE / 2][beaconY + GRID_SIZE / 2] = 'G';

        //clear space around GOAL
        List<Location> beaconCirc = generateCircleCoords(ROBOT_WIDTH / 2, new Location(beaconX + GRID_SIZE / 2, beaconY + GRID_SIZE / 2));
        for (Location loc : beaconCirc) {
            if (loc.getX() < 0 || loc.getX() >= GRID_SIZE) continue;
            if (loc.getY() < 0 || loc.getY() >= GRID_SIZE) continue;
            if (MAP[loc.getX()][loc.getY()] == '.')
                MAP[loc.getX()][loc.getY()] = ' ';
        }

        Location start = new Location(GRID_SIZE / 2, GRID_SIZE / 2);
        Location end = new Location(beaconX + GRID_SIZE / 2, beaconY + GRID_SIZE / 2);
        for (Location location : generateShortestPath(start, end))
            MAP[location.getX()][location.getY()] = '=';

        //draw MAP
        projectGrid(MAP);
    }

    private static List<Location> generateShortestPath(Location start, Location end) {
        List<Location> path = new ArrayList<>();
        open = new TreeSet<>(Comparator.comparingInt(Node::getfCost));
        closed = new ArrayList<>();

        Node startNode = Node.getNode(start);
        startNode.setfCost(0);
        open.add(startNode);

        while (!open.isEmpty()) {
            Iterator<Node> openItr = open.iterator();
            Node current = openItr.next();
            if(current.getLocation().equals(GOAL)) break;
            openItr.remove();
            closed.add(current);

            for (Location adjLoc : adjacentLocations(current.getLocation()))
                checkAndUpdate(current, Node.getNode(adjLoc), current.getfCost() + 10);
            for (Location diagLoc : diagLocations(current.getLocation()))
                checkAndUpdate(current, Node.getNode(diagLoc), current.getfCost() + 14);
        }

        Node endNode = Node.getNode(end);
        while (endNode.getParent() != null) {
            path.add(endNode.getParent().getLocation());
            endNode = endNode.getParent();
        }

        for(int x = 0; x < GRID_SIZE; x++) {
            for(int y = 0; y < GRID_SIZE; y++) {
                //MAP[x][y] = (char) Node.getNode(new Location(x, y)).getfCost();
            }
        }

        Collections.reverse(path);

        return path;
    }

    private static void checkAndUpdate(Node current, Node node, int newG) {
        if (!isWithinBounds(node.getLocation()) ||
                (MAP[node.getLocation().getX()][node.getLocation().getY()] != ' ' && MAP[node.getLocation().getX()][node.getLocation().getY()] != 'G') ||
                closed.contains(node)) {
            return;
        }

        int newF = newG + current.gethCost();

        boolean inOpen = open.contains(node);
        if (!inOpen || newF < node.getfCost()) {
            node.setParent(current);
            node.setfCost(newF);

            if(!inOpen)
                open.add(node);
        }
    }

    private static List<Location> generateCircleCoords(int radius, Location origin) {
        List<Location> circle = new ArrayList<>();
        for (int x = -radius; x <= radius; x++) {
            int yMax = (int) Math.round(Math.sqrt((radius * radius) - (x * x)));
            int yMin = -yMax;

            for (int y = yMin; y <= yMax; y++)
                circle.add(origin.add(x, y));
        }
        return circle;
    }

    private static void drawLine(Line line, char[][] map) {
        if (Math.abs(line.getSlope()) >= 1) {
            int minY = Math.min(line.getStart().getY(), line.getEnd().getY());
            int maxY = Math.max(line.getStart().getY(), line.getEnd().getY());
            int x1, x2;
            if (minY == line.getStart().getY()) {
                x1 = line.getStart().getX();
                x2 = line.getEnd().getX();
            } else {
                x1 = line.getEnd().getX();
                x2 = line.getStart().getX();
            }

            for (int y = minY; y < maxY; y++) {
                int x = (int) Math.round(((double) (y - minY) * (x2 - x1)) / (maxY - minY) + x1);
                if (map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != ' ') continue;
                map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'A';
            }
        } else {
            int minX = Math.min(line.getStart().getX(), line.getEnd().getX());
            int maxX = Math.max(line.getStart().getX(), line.getEnd().getX());
            int y1, y2;
            if (minX == line.getStart().getX()) {
                y1 = line.getStart().getY();
                y2 = line.getEnd().getY();
            } else {
                y1 = line.getEnd().getY();
                y2 = line.getStart().getY();
            }

            for (int x = minX; x < maxX; x++) {
                int y = (int) Math.round(((double) (y2 - y1) / (maxX - minX)) * (x - minX) + y1);
                if (map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != ' ') continue;
                map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'A';
            }
        }
    }

    public static int[] fakeData(int distMin, int distMax, int jumpSmall, int jumpLarge, int numValues) {
        int n = (distMax - distMin) / 2;
        int[] nums = new int[numValues];
        int npos = 0;
        for (int i = 0; i < numValues; i++) {
            n += (Math.random() * jumpSmall - (jumpSmall / 2));
            if ((int) (Math.random() * 20) == 10) {
                n += (Math.random() * jumpLarge - (jumpSmall / 2));
            }
            if (n < distMin || n > distMax) n = (distMax - distMin) / 2;
            nums[npos++] = n;
        }
        return nums;
    }

    private static void projectGrid(char[][] map, int newGridSize) {
        int gridSize = map.length;
        int ratio = (int) Math.round(gridSize / (double) newGridSize);

        char[][] newMap = new char[newGridSize][newGridSize];

        for (int i = 0; i < map.length - ratio; i += ratio) {
            for (int j = 0; j < map.length - ratio; j += ratio) {
                int xCount = 0;
                for (int x = i; x < i + ratio; x++)
                    for (int y = j; y < j + ratio; y++)
                        if (map[x][y] == 'X')
                            xCount++;

                if (xCount >= ratio * ratio / gridSize + 1)
                    newMap[i / ratio][j / ratio] = 'X';
                else
                    newMap[i / ratio][j / ratio] = ' ';
            }
        }

        for (int y = newMap[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < newMap.length; x++) {
                System.out.print(newMap[x][y] + " ");
            }
            System.out.println();
        }
    }

    private static void projectGrid(char[][] map) {
        for (int y = map[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < map.length; x++) {
                System.out.print(map[x][y] + " ");
            }
            System.out.println();
        }
    }

    public static boolean isWithinBounds(Location location) {
        if (location.getX() < 0 || location.getX() >= GRID_SIZE) return false;
        if (location.getY() < 0 || location.getY() >= GRID_SIZE) return false;
        return true;
    }

    public static List<Location> adjacentLocations(Location loc) {
        List<Location> list = new ArrayList<>();
        list.add(loc.add(0, 1));
        list.add(loc.add(1, 0));
        list.add(loc.add(-1, 0));
        list.add(loc.add(0, -1));
        return list;
    }

    public static List<Location> diagLocations(Location loc) {
        List<Location> list = new ArrayList<>();
        list.add(loc.add(-1, 1));
        list.add(loc.add(1, 1));
        list.add(loc.add(-1, -1));
        list.add(loc.add(1, -1));
        return list;
    }

}