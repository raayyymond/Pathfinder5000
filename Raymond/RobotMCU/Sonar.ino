void setupSonar() {
  pinMode(14, INPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
}

int readSonar(int sonarNum) {
  delay(20);
  return 13;
  
  int trigPin = (sonarNum == 0) ? 12 : 13;
  int echoPin = 14;
  
  unsigned long pulse;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(1);
  pulse = pulseIn(echoPin, HIGH);
  
  delay(20);
  
  return round(pulse / 58.2);
}
