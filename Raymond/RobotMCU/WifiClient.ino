#include <SPI.h>
#include <ESP8266WiFi.h>

char ssid[] = "PosSys Tower";
char pass[] = "pathfinder";

IPAddress tower(192,168,4,1);
WiFiClient client;
WiFiServer server(81);

String message = "";

void setupWifi() {
  Serial.begin(115200);
  WiFi.begin(ssid, pass);
  WiFi.mode(WIFI_STA);
  Serial.println("\nconnecting to wifi network");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("\nconnected to wifi network\n\n");
  Serial.println("starting server");
  server.begin();
  Serial.println("server started\n\n");
}

boolean hasMessage() {
  WiFiClient client = server.available();
  boolean recieved = false;
  
  if (client) {
    if(client.connected()) {
      message = client.readStringUntil('\r');
      Serial.println("recieved: " + message);
      recieved = true;
    }
    client.stop();
  }

  return recieved;
}

void sendMessage(String msg) {
  client.connect(tower, 80);
  Serial.println("sending: " + msg);
  client.println(msg + "\r");
  client.flush();
}

String getMessage() {
  return message;
}

