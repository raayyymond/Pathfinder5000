#define STEPS_PER_REV 4096
#define NUM_READINGS 512
#define CHART_SIZE 100
#define LASER 15

unsigned long lastLaserPrint = 0;

int locationX, locationY;

int readings[NUM_READINGS];
char** chart = new char*[CHART_SIZE];
int* pathX;
int* pathY;

const int seperation = 150;

void setup() {
  Serial.begin(115200);
  Serial.println();

  setupWifi();
  setupStepper();
  setupSonar();

  pinMode(LASER, OUTPUT);

  for (int i = 0; i < CHART_SIZE; i++)
    chart[i] = new char[CHART_SIZE];
}

void loop() {
  //updateReadings();
  updateLocation();
  //updateMap();
  //generatePath();
  //followPath();

  delay(3000); //TEMP
}

void followPath() {
  for (int i = 0; i < sizeof(pathX); i++) {
    int finalX = pathX[i];
    int finalY = pathY[i];

    int deltaX = finalX - locationX;
    int deltaY = finalY - locationY;

    float angle = atan2(deltaY, deltaX) * 180.0 / PI;
    int distance = (int) round(sqrt((deltaX * deltaX) + (deltaY * deltaY)));

    turn(angle);
    goForward(distance);
    turn(-angle);

    updateLocation();
  }
}

void generatePath() {

}

void updateMap() {
  for (int i = 0; i < NUM_READINGS; i++) {
    float degreesPerStep = (STEPS_PER_REV / NUM_READINGS) / ((float) STEPS_PER_REV) * 360;
    float angle = degreesPerStep * i * PI / 180;

    int x = readings[i] * cos(angle) + locationX + (CHART_SIZE / 2);
    int y = readings[i] * sin(angle) + locationY + (CHART_SIZE / 2);

    chart[x][y] = 'A';
  }
}

void updateLocation() {
  int steps[] = {0, 0};

  sendMessage("scanning");

  digitalWrite(LASER, HIGH);
  
  for (int i = 0; i < 2; i++) {
    Serial.println("Waiting for laser " + String(i + 1) + " to hit");
    int dir = (i * 2) - 1;
    
    while (!hasMessage()) {
      stepper(STEPS_PER_REV / 512 * dir);
      steps[i]++;

      if (millis() - lastLaserPrint >= 100) {
        Serial.print(".");
        Serial.println(getMessage());
        lastLaserPrint = millis();
      }
    }
    Serial.println();

    Serial.println("resetting motor");
    stepper(STEPS_PER_REV / 512 * steps[i] * -dir);
  }

  sendMessage("finished");

  digitalWrite(LASER, LOW);

  float degreesPerStep = (STEPS_PER_REV / NUM_READINGS) / ((float) STEPS_PER_REV) * 360;
  float A = degreesPerStep * steps[0];
  float B = degreesPerStep * steps[1];
  float a = tan(A) * PI / 180;
  float b = tan((2 * PI) - B) * PI / 180;
  float c = -b * seperation;

  float x = c / (a - b);
  float y = a * x;

  locationX = round(x);
  locationY = round(y);
  Serial.println("Updated location: x=" + String(x) + "     y=" + String(y));
  Serial.println("Angle 1=" + String(A) + "     Angle 2=" + String(B));
}

void updateReadings() {
  for (int i = 0; i < NUM_READINGS / 2; i++) {
    readings[i] = readSonar(0);
    readings[i + (NUM_READINGS / 2)] = readSonar(1);

    stepper(STEPS_PER_REV / NUM_READINGS);
  }
  stepper(-STEPS_PER_REV / 2);
}




/*
   Util Methods
*/

void goForward(int distance) {
  boolean forward = distance > 0;
  String returnMessage = "";
  Serial.print('m');
  Serial.print(forward ? 'f' : 'r');
  Serial.print('.');
  char distChars[String(distance).length()];
  strcpy(distChars, String(distance).c_str());
  for (char c : distChars) {
    Serial.print((char) String(c).toInt());
  }
  delay(500 * distance);
  while (!Serial.available()) {}
  while (Serial.available()) {
    returnMessage = returnMessage + Serial.read();
  }
}

void turn(int degree) {
  boolean forward = degree > 0;
  Serial.print('t');
  Serial.print(forward ? 'f' : 'r');
  Serial.print('.');
  char distChars[String(degree).length()];
  strcpy(distChars, String(degree).c_str());
  for (char c : distChars) {
    Serial.print((char) String(c).toInt());
  }

  while (!Serial.available()) {}
  String returnMessage = "";
  while (Serial.available()) {
    returnMessage = returnMessage + Serial.read();
  }
}

String getValue(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
