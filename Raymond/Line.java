public class Line {

    private Location start;
    private Location end;

    public Line(Location start, Location end) {
        this.start = start;
        this.end = end;
    }

    public Location getStart() {
        return start;
    }

    public Location getEnd() {
        return end;
    }

    public double getLength() {
        return Math.sqrt(Math.pow(getStart().getX() - getEnd().getX(), 2) + Math.pow(getStart().getY() - getEnd().getY(), 2));
    }

    public double getSlope() {
        return ((double) (end.getY() - start.getY())) / (end.getX() - start.getX());
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Line))
            return false;
        Line line = (Line) obj;
        return (line.getStart().equals(getStart()) && line.getEnd().equals(getEnd())) ||
                (line.getStart().equals(getEnd()) && line.getEnd().equals(getStart()));
    }

    @Override
    public String toString() {
        return getStart().toString() + " : " + getEnd().toString();
    }
}
