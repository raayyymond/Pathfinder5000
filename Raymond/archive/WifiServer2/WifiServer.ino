#include <SPI.h>
#include <ESP8266WiFi.h>

char ssid[] = "PosSys Tower";
char pass[] = "pathfinder";

IPAddress robot(192,168,4,2);
WiFiClient robotClient;
WiFiServer server(80);

String message = "";

void setupWifi() {
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, pass);
  WiFi.begin();

  Serial.println();
  Serial.println(WiFi.softAPIP());
  Serial.println("\n\nstarting server");
  server.begin();
  Serial.println("server started\n\n");
}

boolean hasMessage() {
  WiFiClient client = server.available();
  boolean recieved = false;
  
  if (client) {
    if(client.connected()) {
      message = client.readStringUntil('\r');
      Serial.println("recieved: " + message);
      recieved = true;
    }
    client.stop();
  }
  
  return recieved;
}

void sendMessage(String msg) {
  robotClient.connect(robot, 81);
  Serial.println("sending: " + msg);
  robotClient.println(msg + "\r");
  robotClient.flush();
}

String getMessage() {
  return message;
}

