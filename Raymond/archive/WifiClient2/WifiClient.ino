#include <SPI.h>
#include <ESP8266WiFi.h>

char ssid[] = "PosSys Tower";
char pass[] = "pathfinder";

IPAddress tower(192,168,4,1);
WiFiClient client;

boolean hasNewMessage = false;
String message;

void setupWifi() {
  Serial.begin(115200);
  WiFi.begin(ssid, pass);
  WiFi.mode(WIFI_STA);
  Serial.println("\nconnecting to wifi network");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("\nconnected to wifi network\n\n");
  Serial.println("connecting to tower");
  client.connect(tower, 80);
  Serial.println("connected to tower\n\n");
}

void runWifi() {
  if (client) {
    if(!alreadyConnected) {
      client.flush();
      alreadyConnected = true;
    }

    if(client.available() > 0) {
      hasNewMessage = true;
      message = client.readStringUntil('\r');
      Serial.println("recieved: " + message);
      client.flush();
    }
  }
}

void sendMessage(String msg) {
  Serial.println("sending: " + msg);
  client.println(msg + "\r");
}

boolean hasMessage() {
  return hasNewMessage;
}

String getMessage() {
  return message;
}

