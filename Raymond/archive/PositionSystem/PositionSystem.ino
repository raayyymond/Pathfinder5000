//Stepper Pins
#define IN1_1  16
#define IN2_1  5
#define IN3_1  4
#define IN4_1  0

#define IN1_2  14
#define IN2_2  12
#define IN3_2  13
#define IN4_2  15

#define STEPS_PER_REV 4096
#define NUM_READINGS 412

int readings1[NUM_READINGS];
int readings2[NUM_READINGS];

int x, y;

void setup() {
  Serial.begin(9600);
  
  pinMode(IN1_1, OUTPUT); 
  pinMode(IN2_1, OUTPUT); 
  pinMode(IN3_1, OUTPUT); 
  pinMode(IN4_1, OUTPUT);
  
  pinMode(IN1_2, OUTPUT); 
  pinMode(IN2_2, OUTPUT); 
  pinMode(IN3_2, OUTPUT); 
  pinMode(IN4_2, OUTPUT);
}

void loop() {
  delay(100000); //TEMP
}

void pingReceived() {
  updateReadings();
  calcLocation();
  sendLocation();
}

void sendLocation() {
  
}

void calcLocation() {
  
}

void updateReadings() {
  for(int x = 0; x < 2; x++) {
    for(int i = 0; i < NUM_READINGS / 2; i++) {
      if(x == 0)
        readings1[i] = readIRStrength(x);
      else
        readings2[i + (NUM_READINGS / 2)] = readIRStrength(x);
      
      stepper(STEPS_PER_REV / NUM_READINGS, x);
    }
    stepper(-STEPS_PER_REV / 2, x);
  }
}

/*
 * Util Methods
 */
int readIRStrength(int id) {
  
}

void stepper(int xw, int id) {
  int IN1, IN2, IN3, IN4;
  switch(id) {
    case 0:
      IN1 = IN1_1;
      IN2 = IN2_1;
      IN3 = IN3_1;
      IN4 = IN4_1;
      break;
    case 1:
      IN1 = IN1_2;
      IN2 = IN2_2;
      IN3 = IN3_2;
      IN4 = IN4_2;
  }
  
  int steps = 0;
  boolean dir = xw > 0;
  xw = abs(xw);
  
  for (int x = 0; x < xw; x++) {
    switch(steps){
     case 0:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, HIGH);
     break; 
     case 1:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, HIGH);
     break; 
     case 2:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, LOW);
     break; 
     case 3:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, LOW);
     break; 
     case 4:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
     case 5:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
       case 6:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
     case 7:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, HIGH);
     break; 
     default:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
    }
  if(dir)
    steps++;
  else
    steps--;

  if(steps>7)
    steps=0;
  else if(steps<0)
    steps=7;
    
  delay(1);
  }
}
