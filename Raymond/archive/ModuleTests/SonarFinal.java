package ModuleTests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SonarFinal {

    static int GRID_SIZE = 10;
    static char[][] MAP;

    public static void main(String[] args) {
        //initiate distances array
        int[] distances = new int[360];

        /*
        TODO: Input readings into distances
         */

        //setup MAP / plane
        MAP = new char[GRID_SIZE][GRID_SIZE];
        for (char[] row : MAP)
            Arrays.fill(row, ' ');


        //convert polar to cartesian coordinates, add to MAP as 'X', add to readings list as coordinate
        for (int i = 0; i < distances.length; i++) {
            int x = (int) Math.round(distances[i] * Math.cos(Math.toRadians(i)));
            int y = (int) Math.round(distances[i] * Math.sin(Math.toRadians(i)));

            MAP[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'X';
        }

        projectGrid(MAP);
    }

    private static void projectGrid(char[][] map) {
        for (int y = map[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < map.length; x++) {
                System.out.print(map[x][y] + " ");
            }
            System.out.println();
        }
    }

}
