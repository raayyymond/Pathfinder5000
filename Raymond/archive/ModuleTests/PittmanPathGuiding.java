package ModuleTests;

import java.util.*;

public class PittmanPathGuiding {

    static int GRID_SIZE = 200;
    static int ROBOT_WIDTH = 22;
    static char[][] MAP;
    static Location GOAL;

    private static List<Location> readings = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        //Stuart's Job
        generateLocations();
        MAP[GRID_SIZE / 2][GRID_SIZE / 2] = 'X';

        System.out.println("Grid without lines and padding");
        projectGrid(MAP);

        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");

        System.out.println("Grid WITH lines and padding");

        /*
        Raymond Job start
         */

        //generate circle around each point

        //for each dot on MAP draw circle
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                if (MAP[x][y] == 'A' || MAP[x][y] == 'X') {
                    List<Location> circleLocs = generateCircleCoords(ROBOT_WIDTH / 2 - 1, new Location(x, y));
                    for (Location circleLoc : circleLocs) {
                        if (circleLoc.getX() < 0 || circleLoc.getX() >= GRID_SIZE) continue;
                        if (circleLoc.getY() < 0 || circleLoc.getY() >= GRID_SIZE) continue;
                        if (MAP[circleLoc.getX()][circleLoc.getY()] != ' ') continue;
                        MAP[circleLoc.getX()][circleLoc.getY()] = '.';
                    }
                }
            }
        }

        //data for beacon, draw beacon
        int heading = 45;
        int distance = 50;
        int beaconX = (int) Math.round(distance * Math.cos(Math.toRadians(heading)));
        int beaconY = (int) Math.round(distance * Math.sin(Math.toRadians(heading)));
        GOAL = new Location(beaconX + GRID_SIZE / 2, beaconY + GRID_SIZE / 2);
        MAP[beaconX + GRID_SIZE / 2][beaconY + GRID_SIZE / 2] = 'G';

        //clear space around beacon
        List<Location> beaconCirc = generateCircleCoords(ROBOT_WIDTH / 2, new Location(beaconX + GRID_SIZE / 2, beaconY + GRID_SIZE / 2));
        for (Location loc : beaconCirc) {
            if (loc.getX() < 0 || loc.getX() >= GRID_SIZE) continue;
            if (loc.getY() < 0 || loc.getY() >= GRID_SIZE) continue;
            if (MAP[loc.getX()][loc.getY()] == '.')
                MAP[loc.getX()][loc.getY()] = ' ';
        }

        //draw MAP
        projectGrid(MAP);
    }

    private static void generateLocations() {
        int[] distances;

        //fill array with circle points in distance array
        distances = fakeData(0, 100, 5, 50, 360);

        //setup MAP / plane
        MAP = new char[GRID_SIZE][GRID_SIZE];
        for (char[] row : MAP)
            Arrays.fill(row, ' ');


        //convert polar to cartesian coordinates, add to MAP as 'X', add to readings list as coordinate
        for (int i = 0; i < distances.length; i++) {
            int x = (int) Math.round(distances[i] * Math.cos(Math.toRadians(i)));
            int y = (int) Math.round(distances[i] * Math.sin(Math.toRadians(i)));

            if (MAP[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != 'X')
                readings.add(new Location(x, y));

            MAP[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'X';
        }
    }

    private static List<Location> generateCircleCoords(int radius, Location origin) {
        List<Location> circle = new ArrayList<>();
        for (int x = -radius; x <= radius; x++) {
            int yMax = (int) Math.round(Math.sqrt((radius * radius) - (x * x)));
            int yMin = -yMax;

            for (int y = yMin; y <= yMax; y++)
                circle.add(origin.add(x, y));
        }
        return circle;
    }

    private static void drawLine(Line line, char[][] map) {
        if (Math.abs(line.getSlope()) >= 1) {
            int minY = Math.min(line.getStart().getY(), line.getEnd().getY());
            int maxY = Math.max(line.getStart().getY(), line.getEnd().getY());
            int x1, x2;
            if (minY == line.getStart().getY()) {
                x1 = line.getStart().getX();
                x2 = line.getEnd().getX();
            } else {
                x1 = line.getEnd().getX();
                x2 = line.getStart().getX();
            }

            for (int y = minY; y < maxY; y++) {
                int x = (int) Math.round(((double) (y - minY) * (x2 - x1)) / (maxY - minY) + x1);
                if (map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != ' ') continue;
                map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'A';
            }
        } else {
            int minX = Math.min(line.getStart().getX(), line.getEnd().getX());
            int maxX = Math.max(line.getStart().getX(), line.getEnd().getX());
            int y1, y2;
            if (minX == line.getStart().getX()) {
                y1 = line.getStart().getY();
                y2 = line.getEnd().getY();
            } else {
                y1 = line.getEnd().getY();
                y2 = line.getStart().getY();
            }

            for (int x = minX; x < maxX; x++) {
                int y = (int) Math.round(((double) (y2 - y1) / (maxX - minX)) * (x - minX) + y1);
                if (map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] != ' ') continue;
                map[x + GRID_SIZE / 2][y + GRID_SIZE / 2] = 'A';
            }
        }
    }

    private static void projectGrid(char[][] map) {
        for (int y = map[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < map.length; x++) {
                System.out.print(map[x][y] + " ");
            }
            System.out.println();
        }
    }

    public static int[] fakeData(int distMin, int distMax, int jumpSmall, int jumpLarge, int numValues) {
        int n = (distMax - distMin) / 2;
        int[] nums = new int[numValues];
        int npos = 0;
        for (int i = 0; i < numValues; i++) {
            n += (Math.random() * jumpSmall - (jumpSmall / 2));
            if ((int) (Math.random() * 20) == 10) {
                n += (Math.random() * jumpLarge - (jumpSmall / 2));
            }
            if (n < distMin || n > distMax) n = (distMax - distMin) / 2;
            nums[npos++] = n;
        }
        return nums;
    }
}

class Location {

    private int x;
    private int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Location add(int x, int y) {
        return new Location(getX() + x, getY() + y);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Location))
            return false;
        Location loc = (Location) obj;
        return loc.getY() == getY() && loc.getX() == getX();
    }

    public String getId() {
        return toString();
    }

    public static Location fromString(String string) {
        return new Location(Integer.valueOf(string.split(" ")[0]), Integer.valueOf(string.split(" ")[1]));
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}

class Line {

    private Location start;
    private Location end;

    public Line(Location start, Location end) {
        this.start = start;
        this.end = end;
    }

    public Location getStart() {
        return start;
    }

    public Location getEnd() {
        return end;
    }

    public double getLength() {
        return Math.sqrt(Math.pow(getStart().getX() - getEnd().getX(), 2) + Math.pow(getStart().getY() - getEnd().getY(), 2));
    }

    public double getSlope() {
        return ((double) (end.getY() - start.getY())) / (end.getX() - start.getX());
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Line))
            return false;
        Line line = (Line) obj;
        return (line.getStart().equals(getStart()) && line.getEnd().equals(getEnd())) ||
                (line.getStart().equals(getEnd()) && line.getEnd().equals(getStart()));
    }

    @Override
    public String toString() {
        return getStart().toString() + " : " + getEnd().toString();
    }
}