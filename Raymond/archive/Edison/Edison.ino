//Stepper Pins
#define IN1  10
#define IN2  11
#define IN3  12
#define IN4  13

#define STEPS_PER_REV 4096
#define NUM_READINGS 512
#define CHART_SIZE 800

#define SONAR_MAX_DIST 300

int locationX, locationY;

int readings[NUM_READINGS];
char** chart = new char*[CHART_SIZE];
int* path;

void setup() {
  Serial.begin(9600);

  for(int i = 0; i < CHART_SIZE; i++)
    chart[i] = new char[CHART_SIZE];
  
  pinMode(IN1, OUTPUT); 
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  pinMode(IN4, OUTPUT); 
  pinMode(7, OUTPUT);
  pinMode(6, INPUT);
  pinMode(5, OUTPUT);
}

void loop() {
  updateReadings();
  //updateLocation();
  //updateMap();
  //generatePath();
  //followPath();

  delay(1000); //TEMP
}

void followPath() {
  
}

void generatePath() {
  
}

void updateMap() {
  for(int i = 0; i < NUM_READINGS; i++) {
    float degreesPerStep = (STEPS_PER_REV / NUM_READINGS) / ((float) STEPS_PER_REV) * 360;
    float angle = degreesPerStep * i * PI / 180;
    int x = readings[i] * cos(angle) + locationX + (CHART_SIZE / 2);
    int y = readings[i] * sin(angle) + locationY + (CHART_SIZE / 2);
    chart[x][y] = 'A';
  }
}

void updateLocation() {
  
}

void updateReadings() {
  for(int i = 0; i < NUM_READINGS / 2; i++) {
    readings[i] = readSonar(0);
    readings[i + (NUM_READINGS / 2)] = readSonar(1);
    stepper(STEPS_PER_REV / NUM_READINGS);
    Serial.print(i); Serial.print(": "); Serial.println(readings[i]);
    Serial.print(i + (NUM_READINGS / 2)); Serial.print(": "); Serial.println(readings[i + (NUM_READINGS / 2)]);

  }
  stepper(-STEPS_PER_REV / 2);
}

int readSonar(int sonarNum) {
  int trigPin = 7 - (sonarNum * 2);
  int echoPin = 6 - (sonarNum * 2);
  unsigned long pulse;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(1);
  pulse = pulseIn(echoPin, HIGH);
  delay(20);
  return round(pulse / 58.2);
}

void stepper(int xw) {
  int steps = 0;
  boolean dir = xw > 0;
  xw = abs(xw);
  
  for (int x = 0; x < xw; x++) {
    switch(steps){
     case 0:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, HIGH);
     break; 
     case 1:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, HIGH);
     break; 
     case 2:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, LOW);
     break; 
     case 3:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, HIGH);
       digitalWrite(IN4, LOW);
     break; 
     case 4:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
     case 5:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, HIGH);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
       case 6:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
     case 7:
       digitalWrite(IN1, HIGH); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, HIGH);
     break; 
     default:
       digitalWrite(IN1, LOW); 
       digitalWrite(IN2, LOW);
       digitalWrite(IN3, LOW);
       digitalWrite(IN4, LOW);
     break; 
    }
  if(dir)
    steps++;
  else
    steps--;

  if(steps>7)
    steps=0;
  else if(steps<0)
    steps=7;
    
  delay(1);
  }
}
