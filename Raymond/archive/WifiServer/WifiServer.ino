#include <ESP8266WiFi.h>
#include <WiFiServer.h>

const char* ssid = "nodemcu";
const char* password = "password";

WiFiServer server(80);

unsigned long lastPrint = 0;

void setup() {
  Serial.begin(9600);
  delay(1);
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);

  WiFi.begin();

  Serial.print("Access Point IP: "); Serial.println(WiFi.softAPIP());

  server.begin();
  Serial.println("Server started");

  // Wait until the client sends some data
  Serial.println("Client connected, awaiting data");
}

void loop() {
  WiFiClient client = server.available();
  while (!client) {
    if (millis() - lastPrint >= 3000) {
      Serial.println("Awaiting client connection...");
      lastPrint = millis();
    }
    return;
  }
  
  while (client.available()) {
    // Read the first line of the request
    String req = client.readStringUntil('\r');
    Serial.print("recieved: "); Serial.println(req);

    // Send the response to the client
    client.println(req);
    Serial.print("sending: "); Serial.println(req);
  }
}

