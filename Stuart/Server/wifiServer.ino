#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "nodemcu";
const char* password = "password";

WiFiUDP Udp;
unsigned int localUdpPort = 4210;  // local port to listen on
char incomingPacket[255];  // buffer for incoming packets

void wifiServerSetup(){
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);

  WiFi.begin();

  Serial.println(WiFi.softAPIP());

  Udp.begin(localUdpPort);
}

int hasPacket(){
  return Udp.parsePacket();
}

String readPacket(){
  Udp.read(incomingPacket, Udp.available());
  String s = incomingPacket;
  for(int i = 0; i < sizeof(incomingPacket); i++) {
    incomingPacket[i] = '\0';
  }
  return s;
}

void udpsend(String s){
  char message[s.length()];
  s.toCharArray(message, 255);
  const char ip[]="192.168.4.2";
  Serial.print("message: ");
  Serial.println(message);
  Udp.beginPacket(ip,localUdpPort);
  Udp.write(message);
  Udp.endPacket();
  return;
}

