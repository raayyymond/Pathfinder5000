import java.util.Arrays;

public class generateMapData {

    public static void main(String[] args) {
        String s = Arrays.toString(fakeData(10, 200, 10, 100, 360));
        System.out.println("{"+s.substring(1, s.length()-1)+"}");
    }

    public static int[] fakeData(int distMin, int distMax, int jumpSmall, int jumpLarge, int numValues){
        int n = (distMax - distMin) / 2;
        int[] nums = new int[numValues];
        int npos = 0;
        for (int i = 0; i < numValues; i++) {
            n += (Math.random() * jumpSmall - (jumpSmall/2));
            if((int)(Math.random() * 20) == 10){
                n += (Math.random() * jumpLarge - (jumpSmall/2));
            }
            if(n < distMin || n > distMax) n = (distMax-distMin) / 2;
            nums[npos++] = n;
        }
        return nums;
    }
}
