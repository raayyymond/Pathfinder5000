import java.util.Arrays;

public class mapFromData {
    public static void main(String[] args) {
        //int[] distances = generateMapData.fakeData(10, 200, 10, 100, 360);
        int[] distances = {98, 94, 98, 99, 102, 97, 94, 93, 91, 94, 115, 112, 110, 114, 117, 119, 114, 113, 117, 118, 121, 123, 119, 123, 123, 126, 124, 124, 128, 132, 136, 140, 137, 134, 138, 141, 140, 143, 142, 139, 140, 137, 137, 140, 143, 141, 145, 141, 136, 135, 138, 141, 143, 138, 187, 188, 184, 182, 186, 187, 184, 181, 185, 185, 184, 185, 185, 181, 182, 186, 188, 191, 188, 183, 186, 181, 182, 185, 184, 183, 178, 181, 177, 181, 184, 187, 191, 192, 189, 184, 95, 99, 171, 167, 165, 161, 160, 157, 161, 161, 159, 158, 161, 158, 157, 157, 161, 161, 164, 160, 159, 159, 161, 160, 160, 162, 162, 161, 162, 157, 158, 158, 156, 152, 148, 151, 151, 148, 150, 145, 141, 144, 158, 158, 158, 155, 156, 159, 157, 161, 164, 167, 165, 166, 166, 162, 159, 154, 154, 158, 160, 157, 160, 156, 180, 175, 171, 167, 164, 168, 167, 165, 160, 163, 163, 167, 167, 170, 171, 169, 173, 176, 176, 176, 177, 179, 180, 176, 180, 178, 182, 184, 183, 184, 179, 182, 182, 182, 183, 184, 187, 182, 180, 183, 186, 188, 184, 185, 187, 182, 178, 176, 171, 171, 167, 162, 158, 153, 150, 150, 145, 150, 154, 158, 154, 151, 150, 152, 156, 151, 153, 155, 155, 153, 95, 93, 91, 90, 93, 96, 92, 94, 96, 93, 88, 84, 82, 80, 76, 78, 76, 80, 82, 115, 110, 107, 106, 107, 104, 102, 104, 105, 100, 98, 100, 104, 106, 110, 108, 103, 104, 108, 105, 101, 96, 95, 97, 100, 95, 181, 176, 172, 176, 171, 169, 172, 169, 164, 162, 165, 169, 164, 160, 160, 156, 155, 150, 146, 150, 154, 158, 156, 157, 157, 157, 158, 154, 153, 152, 153, 150, 154, 150, 149, 153, 184, 186, 190, 193, 192, 195, 191, 189, 190, 185, 185, 180, 176, 176, 173, 170, 166, 162, 164, 159, 157, 154, 152, 151, 150, 145, 146, 143, 142, 139, 143, 95, 93, 92, 93, 89, 91, 90, 87, 103, 99, 102, 103, 107, 110, 113, 110, 105, 108, 109, 104, 107, 104, 102, 106};

        char[][] map = new char[800][800];
        for (int i = 0; i < map.length; i++) {
            Arrays.fill(map[i], ' ');
        }
        int x, y;
        for (int i = 0; i < 360; i++) {
            x = (int)Math.round(distances[i]*Math.cos(Math.toRadians(i)));
            y = (int)Math.round(distances[i]*Math.sin(Math.toRadians(i)));

            point(map, x, y);
        }
        for(char[] p : map){
            for(char j : p){
                System.out.print(j + " ");
            }
            System.out.println();
        }

    }

    static void point(char[][] map, int x, int y){
        int range = 7;
        placeChar(map, 'X', x, y);
        for (int i = x - range; i <= x + range; i++) {
            for (int j = y - range; j <= y + range; j++) {
                if(isX(map, i, j))
                    drawLine(map, x, y, i, j);
            }
        }
    }

    static boolean placeChar(char[][] map, char c, int x, int y){
        x = x + 400;
        //y = map.length - 1 - y;
        y = y + 400;
        if(map[y][x] != ' ') return false;
        map[y][x] = c;
        return true;
    }
    static boolean isX(char[][] map, int x, int y){
        if(x >= -400 && x < 400 && y >= -400 && y < 400)
            return map[y + 400][x + 400] == 'X';
        return false;
    }

    static void drawLine(char[][] map, int x1, int y1, int x2, int y2){
        int deltaX = Math.abs(x2-x1);
        int deltaY = Math.abs(y2-y1);
        int startX, startY, endX, endY;

        if(deltaX > deltaY){
            if(x2 < x1){
                startX = x2;
                endX = x1;
                startY = y2;
                endY = y1;
            }
            else{
                startX = x1;
                endX = x2;
                startY = y1;
                endY = y2;
            }
            double slope = (double)(endY-startY)/(endX-startX);
            for (int i = 1; i < deltaX; i++) {
                if (!placeChar(map, '0', startX + i, (int)(startY + Math.round(i * slope)))) break;
            }
        }
        else{
            if(y2 < y1){
                startX = x2;
                endX = x1;
                startY = y2;
                endY = y1;
            }
            else{
                startX = x1;
                endX = x2;
                startY = y1;
                endY = y2;
            }
            double slope = (double)(endX-startX)/(endY-startY);
            for (int i = 1; i < deltaY; i++) {
                if (!placeChar(map, '0', (int)(startX + Math.round(i * slope)), startY + i)) break;
            }
        }
    }
}
